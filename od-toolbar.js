import { PolymerElement, html } from '@polymer/polymer';
import ResizeObserver from 'resize-observer-polyfill';
import '@webcomponents/shadycss/entrypoints/apply-shim.js';
import './od-toolbar-item.js';

export class OdToolbar extends PolymerElement {
    static get is() { return 'od-toolbar' }
    static get template() {
        return html`
            <style>
                :host {
                    display: block;
                    width: 100%;
                }

                :host( [has-shadow] ) #toolbar-container {
                    -webkit-box-shadow: 0px 6px 7px 0px rgba(0,0,0,0.5);
                    -moz-box-shadow: 0px 6px 7px 0px rgba(0,0,0,0.5);
                    box-shadow: 0px 6px 7px 0px rgba(0,0,0,0.5);
                }

                :host( [floating][_is-fixed] ) #toolbar-container {
                    position: fixed;
                    z-index: 1;
                    top: 0;
                }

                #toolbar-container {
                    display: table;
                    position: relative;

                    border-top: 1px solid lightgray;
                    border-bottom: 1px solid lightgray;
                    border-radius: 5px;
                    box-sizing: border-box;

                    width: 100%;
                    background: white;

                    @apply --od-toolbar-theme;
                }

                #centre-col > .item-container {
                    justify-content: center;
                }

                #right-col > .item-container {
                    justify-content: flex-end;
                }

                #drop-shadow {
                    position: fixed;
                    height: 2em;
                    width: 100%;
                }

                .od-cell {
                    display: table-cell;
                }

                .item-container {
                    display: flex;
                    height: 100%;
                }

                @media print {
                    :host {                    
                        display: table;
                        position: relative;
                    }
                }

            </style>

            <div id="toolbar-container">
                <div id="left-col" class="od-cell">
                    <div class="item-container">
                        <slot name="left" id="left-slot"></slot>
                    </div>
                </div>
                <div id="centre-col" class="od-cell">
                    <div class="item-container">
                        <slot name="centre" id="centre-slot"></slot>
                    </div>
                </div>
                <div id="right-col" class="od-cell">
                    <div class="item-container">
                        <slot name="right" id="right-slot"></slot>
                    </div>
                </div>
            </div>        
        `;
    }
    static get properties() {
        return {
            floating: {
                type: Boolean
            },

            loadRoute: {
                type: String,
                observer: '_setSelected'
            },

            loadedRoute: {
                type: String,
                notify: true
            },

            _isFixed: {
                type: Boolean,
                reflectToAttribute: true,
                observer: '_setStyles'
            },

            _selected: {
                type: Array,
                value: []
            },

            _items: {
                type: Array,
                value: []
            },

            _isMobile: Boolean,

            _isPortrait: Boolean
        }
    }

    connectedCallback() {
        super.connectedCallback();
        window.addEventListener( 'scroll', () => this._odScrollListener() );
        this._addResizeListener();
    }

    disconnectedCallback() {
        super.disconnectedCallback();
        window.removeEventListener( 'scroll', () => this._odScrollListener() );
        window.removeEventListener( 'od-resize-event', ( e ) => this._updateToolbarListener( e ) );
    }

    ready() {
        super.ready();
        this.addEventListener( 'od-toolbar-item-click', this._setRoute );
        this._getAppSizes();
    }

    _addResizeListener() {
        window.addEventListener( 'od-resize-event', ( e ) => this._updateToolbarListener( e ) );
        var ro = new ResizeObserver( entries => {
            for ( let entry of entries ) {
                this._getAppSizes();
            }
        });
        ro.observe( this );                
    }

    _getAppSizes() {
        this.dispatchEvent( new CustomEvent( 'od-request-sizes', 
            { 
                composed: true, 
                bubbles: true,
                'detail': {
                    'callback': ( appWidth, pageHeightComputed, isPortrait, isMobile ) => {
                        this._isMobile = isMobile;
                        this._isPortrait = isPortrait;
                        this._updateToolbar();
                    }
                } 
            } 
        ) );
    }

    _updateToolbarListener( e ) {                
        this._isMobile = e.detail.isMobile;
        this._isPortrait = e.detail.isPortrait;
        this._updateToolbar();              
    }

    _updateToolbar() {
        if( this._items.length < 1 ) {
            this._getItems();
        }
        this._setSelected();
        this._checkIsFixed();                          
    }

    _getItems() {
        if( !this.shadowRoot ) {
            return;
        }
        
        var cSlot = this.shadowRoot.querySelector( '#centre-slot' );
        var lSlot = this.shadowRoot.querySelector( '#left-slot' );
        var rSlot = this.shadowRoot.querySelector( '#right-slot' );
        var centreItems = cSlot.assignedNodes( {flatten:true} ).filter( n => n.nodeType === Node.ELEMENT_NODE && n.tagName.toLowerCase() === 'od-toolbar-item' );
        var leftItems = lSlot.assignedNodes( {flatten:true} ).filter( n => n.nodeType === Node.ELEMENT_NODE && n.tagName.toLowerCase() === 'od-toolbar-item' );
        var rightItems = rSlot.assignedNodes( {flatten:true} ).filter( n => n.nodeType === Node.ELEMENT_NODE && n.tagName.toLowerCase() === 'od-toolbar-item' );

        if ( centreItems.length < 1 ) {
            this.shadowRoot.querySelector( '#centre-col' ).style.disply = 'none';
        }
        if ( leftItems.length < 1 && rightItems.length < 1 ) {
            this.shadowRoot.querySelector( '#left-col' ).style.display = 'none';
            this.shadowRoot.querySelector( '#right-col' ).style.display = 'none';
        }

        var temp = [];
        Array.prototype.push.apply( temp, centreItems );
        Array.prototype.push.apply( temp, leftItems );
        Array.prototype.push.apply( temp, rightItems );
        if( temp.length < 1 || temp.length === this._items.length ) {
            return;
        }
        this._items = [];
        this._items.length = 0;
        this._items = temp;
    }

    _odScrollListener() {
        this._checkIsFixed();                            
    }

    _checkIsFixed() {
        if( !this.floating ) {
            this._isFixed = false;
            return;
        }
        var shouldBeFixed = window.scrollY > this._getTopPageOffset();           
        if ( this._isFixed === false && shouldBeFixed ) {
            this._isFixed = true;
        } else if ( this._isFixed === true && !shouldBeFixed ) {
            this._isFixed = false;
        } else if ( this._isFixed === null || this._isFixed === undefined ) {
            this._isFixed = shouldBeFixed;
        }
    }

    _setStyles() {                
        var toolbar = this.shadowRoot.querySelector( '#toolbar-container' );
        toolbar.style.height = 'unset';
        if( !this._isFixed ) {
            this.style.paddingTop = 'unset';
            return;
        }
        this.style.paddingTop = toolbar.offsetHeight + 'px';
    }

    _getTopPageOffset() {
        var bodyRect = document.body.getBoundingClientRect();
        var toolBarRect = this.getBoundingClientRect();
        var offset = toolBarRect.top - bodyRect.top;
        return offset;
    }

    _setRoute( e ) {
        e.stopPropagation();
        var route = e.detail.route;
        if ( !route || route === '' ) {
            return;
        }
        this.loadRoute = route;                
    }

    _setSelected() {
        for ( var i in this._selected ) {
            var item = this._selected[i];
            item._isSelected = false;
        }
        this._selected = [];
        if ( !this.loadRoute || this.loadRoute === '' ) {
            return;
        }
        this.loadedRoute = this.loadRoute;
        var tempLoaded = this.loadedRoute; //formatting for precision
        if ( tempLoaded[0] !== '/' ) {
            tempLoaded = '/' + tempLoaded;
        } 
        for ( var i in this._items ) {
            var item = this._items[i];
            var temp = item.route;
            if( !temp ) {
                continue;
            }
            if ( temp[0] !== '/' ) {
                temp = '/' + temp;
            }                    
            if ( item.route === tempLoaded ) {
                item._isSelected = true;
                this._selected.push( item );
                break;
            } else if ( this._childrenRoutesContain( item, tempLoaded ) ) {
                item._isSelected = true;
                this._selected.push( item );
            }
        }
    }

    _childrenRoutesContain( item, route ) {
        var items = item.shadowRoot
                        .querySelector( '#items-slot' )
                        .assignedNodes( {flatten:true} )
                        .filter( n => n.nodeType === Node.ELEMENT_NODE );
        if ( !items || items.length < 1 ) {
            var temp = item.route;
            if ( temp[0] !== '/' ) {
                temp = '/' + temp;
            } 
            if ( route.includes( temp ) ) {
                return true;
            }
            return false;
        }                
        for ( var i in items ) {
            var nestedItem = items[i];

            if ( this._childrenRoutesContain( nestedItem, route ) ) {
                nestedItem._isSelected = true;
                this._selected.push( nestedItem );
                return true;                        
            }
        }
        return false;
    }
}
customElements.define( OdToolbar.is, OdToolbar );