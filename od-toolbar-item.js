import { PolymerElement, html } from '@polymer/polymer';
import { afterNextRender } from '@polymer/polymer/lib/utils/render-status.js';
import '@webcomponents/shadycss/entrypoints/apply-shim.js';
import '@orcden/od-cognito-auth/od-cognito-user-details.js';

export class OdToolbarItem extends PolymerElement {
    static get is() { return 'od-toolbar-item' }
    static get template() {
        return html`
            <style>
                :host {
                    display: inline-flex;
                    cursor: pointer;
                    flex-direction: column;
                }

                :host( [auth] [_authenticated] ) > #title {
                    display: none;
                }

                :host( [auth] :not( [_authenticated] ) ) > #auth-title {
                    display: none;
                }             

                :host( :not( [auth] ) ) > #auth-title {
                    display: none;
                }                      

                :host( [_is-selected] ) > .title ::slotted(*) {
                    border-top: 3px solid #304a89;
                    color: #304a89;

                    @apply --od-toolbar-item-hover-theme;
                }                   

                :host( [_has-menu][open-on-click][_clicked] ) > #container-shim > #items-container {
                    visibility: visible;
                    opacity: 1;
                }

                :host( [_menu-left] ) #items-container {
                    right: 0;
                }

                :host( [has-shadow] ) #items-container {
                    -webkit-box-shadow: 3px 6px 7px 0px rgba(0,0,0,0.5);
                    -moz-box-shadow: 3px 6px 7px 0px rgba(0,0,0,0.5);
                    box-shadow: 3px 6px 7px 0px rgba(0,0,0,0.5);
                }

                :host( [_has-menu]:not( [open-on-click] ) ) {
                    cursor: default;
                }

                :host( :hover ) > .title ::slotted(*) {
                    border-top: 3px solid #304a89;
                    color: #304a89;

                    @apply --od-toolbar-item-hover-theme;
                }            
                
                :host( [_has-menu]:not( [open-on-click] ):hover ) > #container-shim > #items-container {
                    visibility: visible;
                    opacity: 1;
                }  

                .title {
                    display: flex;
                    align-items: center;
                    justify-content: center;
                    width: 100%;
                    height: 100%;
                }

                .title ::slotted(*) {
                    font-family: "Open Sans", Arial, Helvetica, sans-serif;
                    color: #848b8e;
                    background: white;
                    border: none;
                    border-top: 3px solid white;
                    margin-right: 1em;
                    margin-left: 1em;
                    box-sizing: border-box;
                    height: 100%;
                    transition: border .2s ease-in-out;

                    @apply --od-toolbar-item-theme;
                }            

                #container-shim {
                    display: block;
                    position: relative;
                }

                #items-container {
                    display: flex;
                    flex-direction: column;
                    position: absolute;
                    z-index: 1;

                    background: #f2efef;
                    border-radius: 1px;
                    border: none;
                    border-top: 3px solid #304a89;
                    box-sizing: border-box;

                    visibility: hidden;
                    opacity: 0;

                    transition: all .2s ease-in-out;

                    @apply --od-toolbar-item-menu-theme;
                }

                /*for _nested item styles*/
                :host( [_nested] ) {
                    flex-direction: row;
                }

                :host( [_nested]) > .title ::slotted(*) {
                    border: none;
                    border-bottom: 1px solid #dcdadb;
                    background: #f2efef;

                    margin: 0;
                    padding-left: 1em;
                    padding-right: 1em;

                    justify-content: flex-start;
                    min-width: 15em;

                    @apply --od-toolbar-_nested-item-theme;
                }

                :host( [_nested] ) #items-container {
                    @apply --od-toolbar-_nested-item-menu-theme;
                }

                :host( [_nested][_menu-left] ) {
                    flex-direction: row-reverse;
                }            

                :host( [_nested][_is-selected]) > .title ::slotted(*) {
                    background: #f8f8f8;
                    @apply --od-toolbar-_nested-item-hover-theme;
                }

                :host( [_nested]:hover ) > .title ::slotted(*) {
                    background: #f8f8f8;
                    @apply --od-toolbar-_nested-item-hover-theme;
                }      

            </style>

            <od-cognito-user-details authenticated="{{_authenticated}}"></od-cognito-user-details>
            <div id="auth-title" class="title">
                <slot name="auth-title" id="auth-title-slot"></slot>
            </div>
            <div id="title" class="title">
                <slot name="title" id="title-slot"></slot>
            </div>
            <div id="container-shim">
                <div id="items-container">
                    <slot name="items" id="items-slot"></slot>
                </div>
            </div>
        `;
    }
    static get properties() {
        return {
            route: {
                type: String,
                reflectToAttribute: true
            },

            auth: {
                type: Boolean,
                reflectToAttribute: true
            },

            openOnClick: {
                type: Boolean,
                value: false,
                reflectToAttribute: true
            },

            _authenticated: {
                type: Boolean,
                reflectToAttribute: true,
                observer: '_updateItem'
            },

            _clicked: {
                type: Boolean,
                value: false,
                reflectToAttribute: true
            },

            _nested: {
                type: Boolean,
                value: false,
                reflectToAttribute: true
            },

            _menuLeft: {
                type: Boolean,
                value: false,
                reflectToAttribute: true
            },

            _hasMenu: {
                type: Boolean,
                value: false,
                reflectToAttribute: true
            },

            _isSelected: {
                type: Boolean,
                value: false,
                reflectToAttribute: true
            },

            _items: {
                type: Array,
                value: []
            }
        };
    }

    constructor() {
        super();
        this._setClass();
    }

    connectedCallback() {
        super.connectedCallback();
        window.addEventListener( 'od-resize-event', () => this._getAppSizes() );
    }

    disconnectedCallback() {
        super.disconnectedCallback();                
        window.removeEventListener( 'od-resize-event', () => this._getAppSizes() );
    }

    ready() {
        super.ready();
        this._items = [];
        this._items.length = 0;          
        this._calculateNested();
        afterNextRender(this, () => { this._getAppSizes(); });
    }

    _getAppSizes() {
        this.dispatchEvent( new CustomEvent( 'od-request-sizes', 
            { 
                composed: true, 
                bubbles: true,
                'detail': {
                    'callback': ( appWidth, pageHeightComputed, isPortrait, isMobile ) => {               
                        this._calculateMenuLeft( appWidth );
                    }
                } 
            } 
        ) );
    }

    _setClass() {
        this.classList.add( 'od-toolbar-item' );
    }

    _calculateNested() {
        this._items = this.shadowRoot
            .querySelector( '#items-slot' )
            .assignedNodes( {
                flatten: true
            } )
            .filter( n => n.nodeType === Node.ELEMENT_NODE  && n.classList.contains( 'od-toolbar-item' ) );;

        for ( var i in this._items ) {
            var item = this._items[i];
            item._nested = true;
            this._hasMenu = true;
            this._setChildRoutes( item );                        
        }
        if( !this._hasMenu ) {
            this.addEventListener( 'click', this._setClicked );
        }
    }

    _setChildRoutes( item ) {
        if ( this.route !== undefined && this.route !== null ){
            var temp = item.route;
            item.route = this.route + '/' + temp;
        }
    }

    _calculateMenuLeft( appWidth ) {
        var pageWidth = document.documentElement.clientWidth;
        if( appWidth !== undefined ) {
            pageWidth = appWidth;
        }
        var itemContainer = this.shadowRoot.querySelector( '#items-container' ).getBoundingClientRect();
        var thisContainer = this.getBoundingClientRect();
        if( thisContainer.width === 0 ) {
            afterNextRender( this, () => { this._getAppSizes(); });
            return;
        }
        if( this._items.length > 0 && itemContainer.width === 0 ) {
            afterNextRender( this, () => { this._getAppSizes(); });
            return;
        }
        if ( itemContainer.right > pageWidth - 20 ) {              
            this._menuLeft = true;
        }
    }

    _setClicked( e ) {
        e.stopPropagation();
        this._clicked = !this._clicked;
        this.dispatchEvent( new CustomEvent( 'od-toolbar-item-click', {
            bubbles: true,
            composed: true,
            'detail': {
                'route': this.route
            }
        } ) );
    }

    _updateItem() {
        if( !this.auth ) {
            return;
        }
        if( this._authenticated === false ) {
            this._deleteItems();
            return;
        }
    }

    _deleteItems() {                
        var items = this.shadowRoot
            .querySelector( '#items-slot' )
            .assignedNodes( {
                flatten: true
            } )
            .filter( n => n.nodeType === Node.ELEMENT_NODE );
        for ( var i in items ) {
            var item = items[i];
            item.parentNode.removeChild( item );
        }
    }
}
customElements.define( OdToolbarItem.is, OdToolbarItem );